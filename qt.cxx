#include <Qt/QtGui>

/* TODO:
 * - custom parameters
 * - keyboard shortcuts
 */
#ifdef QTOPIA
#include <QtopiaApplication>
#include <QSoftMenuBar>
#endif

#include <cassert>
#include <sys/time.h>

extern "C" {
#include "puzzles.h"
}

#include "qt.h"

// stubs to allow *not* supporting printing

void document_add_puzzle(document *doc, const game *game, game_params *par,
			 game_state *st, game_state *st2)
{
    assert(0);
}

/* ----------------------------------------------------------------------
 * Error reporting functions used elsewhere.
 */

void fatal(char *fmt, ...)
{
    va_list ap;

    fprintf(stderr, "fatal error: ");

    va_start(ap, fmt);
    vfprintf(stderr, fmt, ap);
    va_end(ap);

    fprintf(stderr, "\n");
    exit(1);
}

/* ----------------------------------------------------------------------
 * Qt front end to puzzles.
 */

struct blitter {
    QPixmap *pixmap;
    int w, h, x, y;
};

static void do_start_draw(frontend *fe)
{
    bool ok = fe->painter->begin(fe->pixmap);
    fe->painter->setRenderHints(QPainter::Antialiasing, true);
    assert(ok);
}
static void do_end_draw(frontend *fe)
{
    bool ok = fe->painter->end();
    assert(ok);
}

static void setup_backing_store(frontend *fe)
{
    fe->pixmap = new QPixmap(fe->pw, fe->ph);

    do_start_draw(fe);
    fe->painter->eraseRect(0, 0, fe->pw, fe->ph);
    do_end_draw(fe);
}

static int backing_store_ok(frontend *fe)
{
    return (!!fe->pixmap);
}

static void teardown_backing_store(frontend *fe)
{
    delete fe->pixmap;
    fe->pixmap = NULL;
}

static void repaint_rectangle(frontend *fe, QPaintDevice *widget,
			      int x, int y, int w, int h)
{
    QPainter painter(widget);
    if (x < fe->ox) {
	painter.eraseRect(x, y, fe->ox - x, h);
	w -= (fe->ox - x);
	x = fe->ox;
    }
    if (y < fe->oy) {
	painter.eraseRect(x, y, w, fe->oy - y);
	h -= (fe->oy - y);
	y = fe->oy;
    }
    if (w > fe->pw) {
	painter.eraseRect(x + fe->pw, y, w - fe->pw, h);
	w = fe->pw;
    }
    if (h > fe->ph) {
	painter.eraseRect(x, y + fe->ph, w, h - fe->ph);
	h = fe->ph;
    }
    painter.drawPixmap(QPoint(x, y), *fe->pixmap,
                       QRect(x - fe->ox, y - fe->oy, w, h));
}

Canvas::Canvas(frontend* fe): QWidget(), fe(fe) { }
void Canvas::paintEvent (QPaintEvent *event)
{
    const QRect & rect = event->rect();
    repaint_rectangle(fe, fe->area,
                      rect.x(), rect.y(),
                      rect.width(), rect.height());
}

void Canvas::do_resize(const QSize & size)
{
    int w, h;

    if (backing_store_ok(fe))
        teardown_backing_store(fe);

    w = fe->w = size.width();
    h = fe->h = size.height();
    midend_size(fe->me, &w, &h, TRUE);
    fe->pw = w;
    fe->ph = h;
    fe->ox = (fe->w - fe->pw) / 2;
    fe->oy = (fe->h - fe->ph) / 2;

    setup_backing_store(fe);
    midend_force_redraw(fe->me);
}
void Canvas::resizeEvent (QResizeEvent *event)
{
    do_resize(event->size());
}

void Canvas::mousePressEvent (QMouseEvent *event)
{
    int button;
    if (!backing_store_ok(fe))
        event->ignore();
    switch(event->button()) {
    case Qt::LeftButton: button = LEFT_BUTTON; break;
    case Qt::RightButton: button = RIGHT_BUTTON; break;
    case Qt::MidButton: button = MIDDLE_BUTTON; break;
    default: event->ignore(); return;  /* don't even know what button! */
    }
    event->accept();
    if (!midend_process_key(fe->me, event->x() - fe->ox,
                            event->y() - fe->oy, button))
        qApp->quit();
}
void Canvas::mouseReleaseEvent (QMouseEvent *event)
{
    int button;
    if (!backing_store_ok(fe))
        event->ignore();
    switch(event->button()) {
    case Qt::LeftButton: button = LEFT_RELEASE; break;
    case Qt::RightButton: button = RIGHT_RELEASE; break;
    case Qt::MidButton: button = MIDDLE_RELEASE; break;
    default: event->ignore(); return;  /* don't even know what button! */
    }
    event->accept();
    if (!midend_process_key(fe->me, event->x() - fe->ox,
                            event->y() - fe->oy, button))
        qApp->quit();
}
void Canvas::mouseMoveEvent (QMouseEvent *event)
{
    int button;
    Qt::MouseButtons buttons = event->buttons();
    if (buttons.testFlag(Qt::MidButton))
        button = MIDDLE_DRAG;
    else if (buttons.testFlag(Qt::LeftButton))
        button = LEFT_DRAG;
    else if (buttons.testFlag(Qt::RightButton))
        button = RIGHT_DRAG;
    else {
        event->ignore();    /* don't even know what button! */
        return;
    }
    event->accept();
    if (!midend_process_key(fe->me, event->x() - fe->ox,
                            event->y() - fe->oy, button))
        qApp->quit();
}


void get_random_seed(void **randseed, int *randseedsize)
{
    struct timeval *tvp = snew(struct timeval);
    gettimeofday(tvp, NULL);
    *randseed = (void *)tvp;
    *randseedsize = sizeof(struct timeval);
}

void frontend_default_colour(frontend *fe, float *output)
{
    // FIXME: should probably ask fe->area, but it looks unavailable
    // at this time ?
    // FIXME: that is *not* white !?
    output[0] = 1.0;
    output[1] = 1.0;
    output[2] = 1.0;
}

static void snaffle_colours(frontend *fe)
{
    const float* colours = midend_colours(fe->me, &fe->ncolours);
    fe->colours = new QColor[fe->ncolours];
    for (int i = 0; i < fe->ncolours; i += 1)
        fe->colours[i].setRgbF(colours[3*i], colours[3*i + 1], colours[3*i + 2]);
}

////

class Timer: public QTimer
{
    frontend* fe;
public:
    struct timeval last_time;
    Timer(frontend* fe): QTimer() {
        this->fe = fe;
    }
protected:
    void timerEvent(QTimerEvent *e) {
	struct timeval now;
	float elapsed;
	gettimeofday(&now, NULL);
	elapsed = ((now.tv_usec - last_time.tv_usec) * 0.000001F +
		   (now.tv_sec - last_time.tv_sec));
        midend_timer(fe->me, elapsed);	/* may clear timer_active */
	last_time = now;
    }
};

void deactivate_timer(frontend *fe)
{
    if (!fe)
	return;			       /* can happen due to --generate */
    fe->timer->stop();
}

void activate_timer(frontend *fe)
{
    if (!fe)
	return;			       /* can happen due to --generate */
    if (!fe->timer->isActive()) {
        fe->timer->start(20);
        gettimeofday(&fe->timer->last_time, NULL);
    }
}

////

static void qt_start_draw(void *handle)
{
    frontend *fe = (frontend *)handle;
    fe->bbox_l = fe->w;
    fe->bbox_r = 0;
    fe->bbox_u = fe->h;
    fe->bbox_d = 0;
    do_start_draw(fe);
}

static void qt_end_draw(void *handle)
{
    frontend *fe = (frontend *)handle;
    do_end_draw(fe);

    if (fe->bbox_l < fe->bbox_r && fe->bbox_u < fe->bbox_d) {
	fe->area->update(fe->bbox_l - 1 + fe->ox,
                         fe->bbox_u - 1 + fe->oy,
                         fe->bbox_r - fe->bbox_l + 2,
                         fe->bbox_d - fe->bbox_u + 2);
    }
}

static void qt_clip(void *handle, int x, int y, int w, int h)
{
    frontend *fe = (frontend *)handle;
    fe->painter->setClipRect(x, y, w, h);
}

static void qt_unclip(void *handle)
{
    frontend *fe = (frontend *)handle;
    fe->painter->setClipRect(0, 0, fe->w, fe->h);
}

static void qt_draw_text(void *handle, int x, int y, int fonttype, int fontsize,
                         int align, int colour, char *text)
{
    frontend *fe = (frontend *)handle;

    QFont font;
    font.setPixelSize(fontsize);
    if (fonttype == FONT_FIXED)
        font.setFamily("Monospace");
    fe->painter->setFont(font);

    QFontMetrics fm = fe->painter->fontMetrics();
    int width = fm.width(text);
    QRect rect(0, 0, width, fontsize);

    int flags = 0;
    if (align & ALIGN_VCENTRE) {
        flags |= Qt::AlignVCenter;
        rect.moveTop(y - fontsize/2);
    } else { // as if (align & ALIGN_VNORMAL)
        // FIXME implemented as bottom, not baseline...
        flags |= Qt::AlignBottom;
        rect.moveBottom(y);
    }
    if (align & ALIGN_HCENTRE) {
        flags |= Qt::AlignHCenter;
        rect.moveLeft(x - width/2);
    } else if (align & ALIGN_HRIGHT) {
        flags |= Qt::AlignRight;
        rect.moveRight(x);
    } else { // as if (align & ALIGN_HLEFT)
        flags |= Qt::AlignLeft;
        rect.moveLeft(x);
    }

    fe->painter->setPen(fe->colours[colour]);
    fe->painter->drawText(rect, flags, text);
}

static void qt_draw_rect(void *handle, int x, int y, int w, int h, int colour)
{
    frontend *fe = (frontend *)handle;
    fe->painter->fillRect(x, y, w, h, fe->colours[colour]);
}

static void qt_draw_line(void *handle, int x1, int y1, int x2, int y2, int colour)
{
    frontend *fe = (frontend *)handle;
    fe->painter->setPen(fe->colours[colour]);
    fe->painter->drawLine(x1, y1, x2, y2);
}

static void qt_draw_polygon(void *handle, int *coords, int npoints,
                            int fillcolour, int outlinecolour)
{
    frontend *fe = (frontend *)handle;
    const QBrush oldbrush = fe->painter->brush();
    if (fillcolour != -1)
        fe->painter->setBrush(fe->colours[fillcolour]);
    QPolygon polygon;
    for (int i=0; i < npoints; i++)
        polygon << QPoint(coords[i*2], coords[i*2 + 1]);
    fe->painter->setPen(fe->colours[outlinecolour]);
    fe->painter->drawPolygon(polygon);
    if (fillcolour != -1)
        fe->painter->setBrush(oldbrush);
}

static void qt_draw_circle(void *handle, int cx, int cy, int radius,
                           int fillcolour, int outlinecolour)
{
    frontend *fe = (frontend *)handle;
    QPainterPath path;
    path.addEllipse(QPoint(cx, cy), radius, radius);
    if (fillcolour != -1)
        fe->painter->fillPath(path, fe->colours[fillcolour]);
    fe->painter->setPen(fe->colours[outlinecolour]);
    fe->painter->drawPath(path);
}

static blitter *qt_blitter_new(void *handle, int w, int h)
{
    blitter *bl = snew(blitter);
    /*
     * We can't create the pixmap right now, because fe->window
     * might not yet exist. So we just cache w and h and create it
     * during the firs call to blitter_save.
     */
    bl->pixmap = NULL;
    bl->w = w;
    bl->h = h;
    return bl;
}

static void qt_blitter_free(void *handle, blitter *bl)
{
    if (bl->pixmap)
        delete bl->pixmap;
    sfree(bl);
}

static void qt_blitter_save(void *handle, blitter *bl, int x, int y)
{
    frontend *fe = (frontend *)handle;

    if (!bl->pixmap) {
        bl->pixmap = new QPixmap;
    }
    bl->x = x;
    bl->y = y;
    *bl->pixmap = fe->pixmap->copy(bl->x, bl->y, bl->w, bl->h);
}

static void qt_blitter_load(void *handle, blitter *bl, int x, int y)
{
    frontend *fe = (frontend *)handle;
    if (x == BLITTER_FROMSAVED && y == BLITTER_FROMSAVED) {
        x = bl->x;
        y = bl->y;
    }
    assert(bl->pixmap);
    fe->painter->drawPixmap(QPoint(x, y), *bl->pixmap);
}

static void qt_draw_update(void *handle, int x, int y, int w, int h)
{
    frontend *fe = (frontend *)handle;
    if (fe->bbox_l > x  ) fe->bbox_l = x  ;
    if (fe->bbox_r < x+w) fe->bbox_r = x+w;
    if (fe->bbox_u > y  ) fe->bbox_u = y  ;
    if (fe->bbox_d < y+h) fe->bbox_d = y+h;
}

const struct drawing_api qt_drawing = {
    qt_draw_text,
    qt_draw_rect,
    qt_draw_line,
    qt_draw_polygon,
    qt_draw_circle,
    qt_draw_update,
    qt_clip,
    qt_unclip,
    qt_start_draw,
    qt_end_draw,
    NULL, //qt_status_bar,
    qt_blitter_new,
    qt_blitter_free,
    qt_blitter_save,
    qt_blitter_load,
    NULL, NULL, NULL, NULL, NULL, NULL, /* {begin,end}_{doc,page,puzzle} */
    NULL, NULL,			       /* line_width, line_dotted */
    NULL, //text_fallback
    NULL, //draw_thick_line,
};

static void maybe_enlarge_window(frontend *fe)
{
    QSize delta = fe->area->sizeHint() - fe->area->size();
    if (delta.width() < 0) delta.rwidth() = 0;
    if (delta.height() < 0) delta.rheight() = 0;
    if (delta == QSize(0, 0))
        fe->area->do_resize(fe->window->size());
    else
        fe->window->resize(fe->window->size() + delta);
}

static void add_type_submenu(QMenu *menu, QWidget *window, frontend *fe, int npreset)
{
    QActionGroup *itemgrp = new QActionGroup(window);
    QSignalMapper *signalMapper = new QSignalMapper(window);
    window->connect(signalMapper, SIGNAL(mapped(QObject*)),
                    window, SLOT(preset(QObject*)));

    for (int i = 0; i < npreset; i++) {
        char *name;
        game_params *params;

        midend_fetch_preset(fe->me, i, &name, &params);

        QAction *a = new QAction(name, window);
        menu->addAction(a);
        window->connect(a, SIGNAL(triggered()), signalMapper, SLOT(map()));
        GameParams *p = new GameParams(params);
        signalMapper->setMapping(a, p);
        itemgrp->addAction(menu->menuAction());
    }
}

PuzzleWindow::PuzzleWindow(QWidget *parent, Qt::WFlags fl)
    : QMainWindow( parent, fl )
{
    fe = snew(frontend);

    if (!fe) {
        fprintf(stderr, "failed to allocate frontend\n");
        exit(1);
    }

    fe->timer = new Timer(fe);

    fe->me = midend_new(fe, &thegame, &qt_drawing, fe);
    midend_new_game(fe->me);

    setWindowTitle(thegame.name);
    QWidget *vbox = new QWidget(this);
    QVBoxLayout *vlayout = new QVBoxLayout(vbox);
    vlayout->setMargin(0);

    fe->pixmap = NULL;
    fe->painter = new QPainter;

#ifdef QTOPIA
    QMenu *menu = QSoftMenuBar::menuFor(this);
#else
    QMenu *menu = this->menuBar()->addMenu("&Game");
#endif
    menu->addAction("New game", this, SLOT(newGame()));
    int npreset;
    if ((npreset = midend_num_presets(fe->me)) > 0 || thegame.can_configure) {
#ifdef QTOPIA
        QMenu *submenu = menu->addMenu("Type");
#else
        QMenu *submenu = this->menuBar()->addMenu("Type");
#endif
        add_type_submenu(submenu, this, fe, npreset);
    }
    menu->addAction("Restart", this, SLOT(restart()));
    menu->addAction("Undo", this, SLOT(undo()));
    menu->addAction("Redo", this, SLOT(redo()));
    menu->addAction("Solve", this, SLOT(solve()));
#ifdef QTOPIA
    menu->addSeparator();
    QMenu *submenu = menu;
#else
    menu->addAction("Exit", qApp, SLOT(quit()));
    QMenu *submenu = this->menuBar()->addMenu("Help");
#endif
    submenu->addAction("About", this, SLOT(about()));

    snaffle_colours(fe);

#ifdef STYLUS_BASED
    if (thegame.flags & REQUIRE_NUMPAD) {
        QSignalMapper *signalMapper = new QSignalMapper(this);
        this->connect(signalMapper, SIGNAL(mapped(int)),
                      this, SLOT(numpad(int)));

        QWidget *hbox = new QWidget(this);
        QHBoxLayout *hlayout = new QHBoxLayout(hbox);
        hlayout->setMargin(0);
        vlayout->addWidget(hbox);

	char buf[2];
        buf[1] = '\0';
	for(buf[0]='0'; buf[0]<='9'; buf[0]++) {
	    QPushButton *button = new QPushButton(buf);
            button->setMinimumSize(1, 0); // FIXME we could do better
            hlayout->addWidget(button);
            this->connect(button, SIGNAL(clicked()), signalMapper, SLOT(map()));
            signalMapper->setMapping(button, buf[0]);
	}
    }
#endif
    fe->area = new Canvas(fe);
    vlayout->addWidget(fe->area);
    setCentralWidget(vbox);

    fe->window = this;
}
QSize Canvas::sizeHint() const {
    int x, y;

    /*
     * Currently I don't want to scale large
     * puzzles to fit on the screen. This is because X does permit
     * extremely large windows and many window managers provide a
     * means of navigating round them, and the users I consulted
     * before deciding said that they'd rather have enormous puzzle
     * windows spanning multiple screen pages than have them
     * shrunk. I could change my mind later or introduce
     * configurability; this would be the place to do so, by
     * replacing the initial values of x and y with the screen
     * dimensions.
     */
    x = INT_MAX;
    y = INT_MAX;
    midend_size(fe->me, &x, &y, FALSE);
    return QSize(x, y);
}
void PuzzleWindow::newGame() {
    midend_process_key(fe->me, 0, 0, 'n');
}
void PuzzleWindow::preset(QObject *o) {
    GameParams *p = qobject_cast<GameParams*>(o);
    assert (p);

    midend_set_params(fe->me, p->params);
    midend_new_game(fe->me);
    //changed_preset(fe);
#ifdef QTOPIA
    fe->area->do_resize(fe->window->size());
#else
    maybe_enlarge_window(fe);
#endif
}
void PuzzleWindow::restart() {
    midend_restart_game(fe->me);
}
void PuzzleWindow::undo() {
    midend_process_key(fe->me, 0, 0, 'u');
}
void PuzzleWindow::redo() {
    midend_process_key(fe->me, 0, 0, 'r');
}
void PuzzleWindow::solve() {
    char *msg;

    msg = midend_solve(fe->me);

    if (msg) {
	QMessageBox mb;
        mb.setText(msg);
        mb.exec();
    }
}
void PuzzleWindow::numpad(int n) {
    midend_process_key(fe->me, 0, 0, (char)n);
}
void PuzzleWindow::about() {
    char titlebuf[256];
    char textbuf[1024];

    sprintf(titlebuf, "About %.200s", thegame.name);
    sprintf(textbuf,
	    "%.200s\n\n"
	    "from Simon Tatham's Portable Puzzle Collection\n\n"
	    "%.500s", thegame.name, ver);

    QMessageBox mb;
    mb.setWindowTitle(titlebuf);
    mb.setText(textbuf);
    mb.exec();
}


#ifdef QTOPIA
QTOPIA_ADD_APPLICATION(QTOPIA_TARGET,PuzzleWindow)
QTOPIA_MAIN
#else
int main(int argc, char **argv)
{
    char *pname = argv[0];
    int doing_opts = TRUE;
    int ac = argc;
    char **av = argv;
    char errbuf[500];

    /*
     * Command line parsing in this function is rather fiddly,
     * because GTK wants to have a go at argc/argv _first_ - and
     * yet we can't let it, because gtk_init() will bomb out if it
     * can't open an X display, whereas in fact we want to permit
     * our --generate and --print modes to run without an X
     * display.
     * 
     * So what we do is:
     * 	- we parse the command line ourselves, without modifying
     * 	  argc/argv
     * 	- if we encounter an error which might plausibly be the
     * 	  result of a GTK command line (i.e. not detailed errors in
     * 	  particular options of ours) we store the error message
     * 	  and terminate parsing.
     * 	- if we got enough out of the command line to know it
     * 	  specifies a non-X mode of operation, we either display
     * 	  the stored error and return failure, or if there is no
     * 	  stored error we do the non-X operation and return
     * 	  success.
     *  - otherwise, we go straight to gtk_init().
     */

    errbuf[0] = '\0';
    while (--ac > 0) {
	char *p = *++av;
	if (doing_opts && !strcmp(p, "--version")) {
	    printf("%s, from Simon Tatham's Portable Puzzle Collection\n%s\n",
		   thegame.name, ver);
	    return 0;
	} else if (doing_opts && !strcmp(p, "--")) {
	    doing_opts = FALSE;
	} else {
	    sprintf(errbuf, "%.100s: unrecognised option '%.100s'\n",
		    pname, p);
	    break;
	}
    }

    if (*errbuf) {
	fputs(errbuf, stderr);
	return 1;
    }

    QApplication app(argc, argv);
    PuzzleWindow w;
    w.show();

    return app.exec();
}
#endif
