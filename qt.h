#include <QtCore/QObject>
#include <QtGui/QMainWindow>

extern "C" {
#include "puzzles.h"
}

class Canvas;
class Timer;

struct frontend {
    QMainWindow *window;
    Canvas *area;
    QPixmap *pixmap;
    QPainter *painter;
    int w, h;
    midend *me;
    QColor *colours;
    int ncolours;
    int bbox_l, bbox_r, bbox_u, bbox_d;
    Timer *timer;
    int pw, ph;                        /* pixmap size (w, h are area size) */
    int ox, oy;                        /* offset of pixmap in drawing area */
};

class Canvas: public QWidget
{
    Q_OBJECT;
    frontend* fe;
  public:
    Canvas(frontend* fe);
    virtual QSize sizeHint() const;
    void do_resize(const QSize & size);
  protected:
    void paintEvent (QPaintEvent *event);
    void resizeEvent (QResizeEvent *event);
    void mousePressEvent (QMouseEvent *event);
    void mouseReleaseEvent (QMouseEvent *event);
    void mouseMoveEvent (QMouseEvent *event);
};

class PuzzleWindow: public QMainWindow {
    Q_OBJECT;
    frontend *fe;
  public:
    PuzzleWindow(QWidget *parent=0, Qt::WFlags fl=0);
  public slots:
    void newGame();
    void restart();
    void undo();
    void redo();
    void solve();
    void about();
    void preset(QObject*);
    void numpad(int);
};

// Simple QObject wrapper around game_params
class GameParams: public QObject {
    Q_OBJECT;
  public:
    GameParams(game_params *params)
        : params(params) {};
    game_params *params;
};
