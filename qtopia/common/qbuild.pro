TEMPLATE = lib
CONFIG = staticlib

CONFIG += qtopia
DEFINES += QTOPIA
DEFINES += STYLUS_BASED

TARGET=common
HEADERS += ../../qt.h
SOURCES = ../../combi.c ../../divvy.c ../../drawing.c ../../dsf.c ../../grid.c ../../latin.c ../../laydomino.c ../../loopgen.c ../../malloc.c ../../midend.c ../../maxflow.c ../../misc.c ../../no-icon.c ../../penrose.c ../../qt.cxx ../../random.c ../../tdq.c ../../tree234.c ../../version.c
